import * as CF from 'colfio';

export type CollisionMessage = {
    projectile: CF.Container;
    enemy: CF.Container;
}

export type NewProjectileMessage = {
    position: CF.Vector;
    rotation: number;
    damage: number;
    multishot: number;
    knockback: number;
    texture: string;
}

export type MoneyMessage = {
    money: number;
}