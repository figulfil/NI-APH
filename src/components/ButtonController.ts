import * as CF from 'colfio';
import * as PIXI from 'pixi.js';
import { Assets, Messages, StatTypes, TEXTURE_SCALE } from '../Constants';
import { MoneyMessage } from '../Messages';
import { StatSystem } from '../systems/StatSystem';

const pricingList: { [key in StatTypes]: number[] } = {
    [StatTypes.DAMAGE]: [100, 200, 500, 1000, 5000, 7500, 10000, 25000],
    [StatTypes.SPEED]: [100, 200, 500, 1000, 5000, 7500, 10000, 25000],
    [StatTypes.MULTISHOT]: [200, 500, 1000, 5000, 7500, 10000, 25000],
    [StatTypes.KNOCKBACK]: [7500, 10000, 25000]
};

export class ButtonController extends CF.Component {
    rank: number;
    type: StatTypes;
    isDisabled: boolean;

    buttonText: PIXI.Text;

    onInit() {
        this.owner.interactive = true;
        this.owner.buttonMode = true;

        this.owner.on('mousedown', () => this.mouseDown());
        this.owner.on('mouseup', () => this.mouseUp());

        this.owner.asSprite().texture.frame = new PIXI.Rectangle(0, 0, 256, 64);

        this.type = this.owner.getAttribute("type") as StatTypes;
        this.rank = 0;
        this.isDisabled = false;

        this.loadText();
        this.loadPrice();
        this.loadMoneyImage();
    }

    loadText() {
        const text = this.owner.getAttribute("text") as string;

        const buttonText = new PIXI.Text(text, { fill: 0xFFFFFF, fontSize: 26, align: 'center' });
        buttonText.position.set(20, 20);
        this.owner.addChild(buttonText);
    }

    loadPrice() {
        this.buttonText = new PIXI.Text(pricingList[this.type][this.rank], { fill: 0xFFFFFF, fontSize: 26, align: 'center' });
        this.buttonText.position.set(190 - 15 * (pricingList[this.type][this.rank].toString().length - 1), 20);
        this.buttonText.resolution = 1;
        this.owner.addChild(this.buttonText);
    }

    updatePrice() {
        this.buttonText.text = pricingList[this.type][this.rank];
        this.buttonText.position.set(190 - 15 * (pricingList[this.type][this.rank].toString().length - 1), 20);
    }

    maxPrice() {
        this.buttonText.text = "MAX";
        this.buttonText.position.set(180, 20);

        this.owner.removeChild(this.owner.getChildByName("pouch"));
    }

    mouseDown() {
        this.owner.asSprite().texture.frame = new PIXI.Rectangle(0, 64, 256, 64);

        if (!this.isDisabled) {
            if (this.scene.findGlobalComponentByName<StatSystem>("StatSystem").money >= pricingList[this.type][this.rank]) {
                this.rank += 1;

                if (this.rank < pricingList[this.type].length) {
                    this.updatePrice();

                    (this.owner.getAttribute("func") as Function)();
                    this.sendMessage(Messages.EXPENDITURE, {
                        money: -pricingList[this.type][this.rank - 1]
                    } as MoneyMessage);
                }
                else {
                    this.maxPrice();

                    (this.owner.getAttribute("func") as Function)();
                    this.sendMessage(Messages.EXPENDITURE, {
                        money: -pricingList[this.type][this.rank - 1]
                    } as MoneyMessage);

                    this.isDisabled = true;
                }
            }
        }
    }

    mouseUp() {
        this.owner.asSprite().texture.frame = new PIXI.Rectangle(0, 0, 256, 64);
    }

    loadMoneyImage() {
        const img = new PIXI.Sprite(PIXI.Texture.from(Assets.COINS));
        img.position.set(210, 15);
        img.scale.set(3);
        img.name = "pouch";
        this.owner.addChild(img);
    }
}