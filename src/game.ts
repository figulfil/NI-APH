import * as CF from 'colfio';
import { SCENE_WIDTH } from './Constants';
import { GameLoader } from './GameLoader';

class Game {
	engine: CF.Engine;
	canvas: HTMLCanvasElement;

	constructor() {
		this.canvas = document.getElementById('game-canvas') as HTMLCanvasElement;

		this.engine = new CF.Engine();
		this.engine.init(this.canvas, {
			width: this.canvas.width,
			height: this.canvas.height,
			resolution: this.canvas.width / SCENE_WIDTH
		});

		GameLoader.loadGame(this.engine);
	}
}

export default new Game();