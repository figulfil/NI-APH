import * as CF from 'colfio';
import * as PIXI from 'pixi.js';
import { Assets, TEXTURE_SCALE, Tags } from './Constants';
import { ProjectileComponent } from './components/ProjectileController';
import { EnemyController } from './components/EnemyController';
import { CharacterController } from './components/CharacterController';
import { AnimationController } from './components/AnimationController';
import { ExplosionController } from './components/ExplosionController';

const enemyConfig = [
    [Assets.ENEMY_1, 100, 75, 180, 75, 85, 50],
    [Assets.ENEMY_2, 400, 100, 175, 85, 70, 100],
    [Assets.ENEMY_3, 1600, 90, 155, 80, 55, 250], 
    [Assets.ENEMY_4, 6400, 90, 190, 75, 75, 500]
];

const combinations: number[][] = [
    [0, 0, 0, 0, 0],
    [0, 0, 0, 1, 1],
    [1, 1, 0, 0, 0],
    [0, 0, 1, 0, 0],
    [0, 1, 0, 1, 0],
    [1, 0, 0, 0, 1],
    [0, 1, 1, 1, 0],
    [1, 0, 1, 0, 1],
    [1, 1, 0, 1, 1],
    [1, 1, 1, 1, 1],
];

export class Factory {
    scene: CF.Scene;
    money: number = 1000;

    constructor(scene: CF.Scene) {
        this.scene = scene;
    }

    loadGameOver() {
        console.log("Breakpoiint");
    }

    loadGame() {
        new CF.Builder(this.scene)
            .withName(Tags.BACKGROUND)
            .asSprite(PIXI.Texture.from(Assets.LEVEL_BACKGROUND))
            .scale(TEXTURE_SCALE * 16, TEXTURE_SCALE * 12)
            .withParent(this.scene.stage)
            .build();
    }

    loadProjectile(position: CF.Vector, roration: number, numberOfShots: number, damage: number, knockback: number, texture: string) {
        for(let i = 0; i < numberOfShots; i++)
        {
            new CF.Builder(this.scene)
            .withName(Tags.PROJECTILE)
            .asSprite(PIXI.Texture.from(texture))
            .scale(-TEXTURE_SCALE, TEXTURE_SCALE)
            .localPos(position)
            .anchor(0.5)
            .withParent(this.scene.stage)
            .withComponent(new ProjectileComponent(roration + ((i % 2 === 0) ? (0.025 * i) : -(0.025 * i)), damage, knockback))
            .build();
        }
    }

    loadWave(speed: number, health: number) {
        let combination = combinations[Math.floor(Math.random() * 10)];
        let coordinates = this.getShuffledCoordinates(combination.length);

        for(let i = 0; i < combination.length; i++) {
            if(health < (enemyConfig[0][1] as number))
                return; 

            if(combination[coordinates[i]])
            {
                let enemyHP = this.resolveEnemyHP(health);
                health = enemyHP[0];

                this.loadEnemy(enemyHP[1], speed, (enemyConfig[enemyHP[1]][1] as number) , 2 * coordinates[i] + 2);
            }
        }
    }

    getShuffledCoordinates(count: number): number[] {
        const sequentialArray: number[] = [];
    
        for (let i = 0; i <= count; i++) {
            sequentialArray.push(i);
        }
        
        for (let i = sequentialArray.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [sequentialArray[i], sequentialArray[j]] = [sequentialArray[j], sequentialArray[i]];
        }
        
        return sequentialArray;
    }

    resolveEnemyHP(health: number)
    {
        for(let i = enemyConfig.length - 1; i >= 0; i--)
        {
            if(health >= (enemyConfig[i][1] as number))
            {
                health -= (enemyConfig[i][1] as number);
                return [health, i];
            }
        }
    }

    loadEnemy(type:number, speed: number, health: number, row: number) {
        new CF.Builder(this.scene)
            .withName(Tags.ENEMY)
            .asSprite(PIXI.Texture.from(enemyConfig[type][0] as string))
            .scale(TEXTURE_SCALE * 4)
            .localPos(new CF.Vector(16, row))
            .anchor(0.5)
            .withParent(this.scene.stage)
            .withComponent(new EnemyController())
            .withAttribute("speed", speed)
            .withAttribute("health", health)
            .withAttribute("value", enemyConfig[type][6])
            .withComponent(new AnimationController())
            .withAttribute("x", enemyConfig[type][2])
            .withAttribute("y", enemyConfig[type][3])
            .withAttribute("width", enemyConfig[type][4])
            .withAttribute("height", enemyConfig[type][5])
            .build();
    }

    loadCharacter(position: CF.Vector) {
        new CF.Builder(this.scene)
            .withName(Tags.PLAYER)
            .asSprite(PIXI.Texture.from(Assets.FRIENDLY_1))
            .scale(-TEXTURE_SCALE * 4, TEXTURE_SCALE * 4)
            .localPos(position)
            .anchor(0.5)
            .withParent(this.scene.stage)
            .withComponent(new CharacterController())
            .withComponent(new AnimationController())
            .build();
    }

    static loadExplosion(scene: CF.Scene, position: CF.Vector) {
        new CF.Builder(scene)
            .withName("Explosion")
            .asSprite(PIXI.Texture.from(Assets.EXPLOSION))
            .scale(TEXTURE_SCALE, TEXTURE_SCALE)
            .localPos(position)
            .anchor(0.5)
            .withParent(scene.stage)
            .withComponent(new ExplosionController())
            .build();
    }

    static loadGameOverScreen(scene: CF.Scene, score: number) {
        scene.clearScene();

        new CF.Builder(scene)
            .withName(Tags.BACKGROUND)
            .asSprite(PIXI.Texture.from(Assets.LEVEL_BACKGROUND))
            .scale(TEXTURE_SCALE * 16, TEXTURE_SCALE * 12)
            .withParent(scene.stage)
            .build();
            
        const menu = new CF.Graphics();

        menu.beginFill(789009);
        menu.drawRoundedRect(4, 3, scene.width / 2, scene.height / 2, 0.5);
        menu.endFill();

        scene.stage.addChild(menu);

        const text = new PIXI.Text("Game Over", { fill: 0xFFFFFF, fontSize: 26, align: 'center' });
        text.position.set(8, 4);
        text.anchor.set(0.5);
        text.scale.set(TEXTURE_SCALE * 10);
        text.resolution = 2;
        scene.stage.addChild(text);

        const scoreValue = new PIXI.Text(score, { fill: 0xFFFFFF, fontSize: 26, align: 'center' });
        scoreValue.position.set(8, 6);
        scoreValue.anchor.set(0.5);
        scoreValue.scale.set(TEXTURE_SCALE * 10);
        scoreValue.resolution = 2;
        scene.stage.addChild(scoreValue);

        const buttonTexture = PIXI.Texture.from(Assets.BUTTON);
        const button = new PIXI.Sprite(buttonTexture);
        button.anchor.set(0.5);
        button.texture.frame = new PIXI.Rectangle(0, 0, 256, 64);
        button.position.set(8, 8);
        button.scale.set(TEXTURE_SCALE * 5);
        
        const buttonText = new PIXI.Text('Play Again', { fontSize: 24, fill: 0xffffff });
        buttonText.anchor.set(0.5);
        buttonText.position.set(button.width / 2, button.height / 2);
        button.addChild(buttonText);
        
        button.interactive = true;
        button.buttonMode = true;
        button.on('pointerdown', () => {
            window.location.reload();
        });
        
        scene.stage.addChild(button);
    }
}