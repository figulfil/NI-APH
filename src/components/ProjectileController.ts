import * as CF from 'colfio';

export class ProjectileComponent extends CF.Component {
	speed = 0.5;
	damage: number;
	knockback: number;
	rotation: number;

	constructor(rotation: number, damage: number, knockback: number) {
		super();

		this.rotation = rotation;
		this.damage = damage;
		this.knockback = knockback;
	}

	onInit() {
		this.fixedFrequency = 60;
		this.owner.rotation = this.rotation;
	}

	onFixedUpdate(delta: number, absolute: number) {
		this.owner.position.x += this.speed * Math.cos(this.owner.rotation);
		this.owner.position.y += this.speed * Math.sin(this.owner.rotation);
	}
}