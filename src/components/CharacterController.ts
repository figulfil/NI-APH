import * as CF from 'colfio';
import * as PIXI from 'pixi.js';
import { Assets, Messages, StatTypes } from '../Constants';
import { NewProjectileMessage } from '../Messages';
import { AnimationController } from './AnimationController';

const skinConfig = [
    [Assets.FRIENDLY_1, 102, 130, 90, 50, Assets.LASER_1],
    [Assets.FRIENDLY_2, 101, 165, 95, 65, Assets.LASER_2],
    [Assets.FRIENDLY_3, 116, 190, 120, 100, Assets.LASER_3],
]

const statList: { [key in StatTypes]: number[] } = {
    [StatTypes.DAMAGE]: [50, 100, 150, 200, 400, 550, 800, 1600, 2150],
    [StatTypes.SPEED]: [200, 300, 400, 500, 600, 700, 800, 900, 1000],
    [StatTypes.MULTISHOT]: [1, 2, 3, 4, 5, 6, 7, 8],
    [StatTypes.KNOCKBACK]: [0, 5, 10, 15],
};

export class CharacterController extends CF.Component {
	damageLevel = 0;
	speedLevel = 0;
	multishotLevel = 0;
	knockbackLevel = 0;

	frame = 0;
	frameCounter = 0;

	cooldown = 0;
	firing = false;

	rank = 0;

	AnimationController: AnimationController;

	onInit() {
		this.fixedFrequency = 60;

		this.subscribe(CF.PointerMessages.POINTER_RELEASE);
		this.subscribe(CF.PointerMessages.POINTER_TAP);
		this.subscribe(CF.PointerMessages.POINTER_DOWN);
		this.subscribe(CF.PointerMessages.POINTER_OVER);

		this.subscribe(Messages.UPGRADE_DAMAGE);
		this.subscribe(Messages.UPGRADE_SPEED);
		this.subscribe(Messages.UPGRADE_MULTISHOT);
		this.subscribe(Messages.UPGRADE_KNOCKBACK);

		this.owner.assignAttribute("x", skinConfig[this.rank][1])
		this.owner.assignAttribute("y", skinConfig[this.rank][2])
		this.owner.assignAttribute("width", skinConfig[this.rank][3])
		this.owner.assignAttribute("height", skinConfig[this.rank][4])

		this.owner.asSprite().texture = PIXI.Texture.from(skinConfig[this.rank][0] as string);
	}

	onUpdate(delta: number, absolute: number): void {
		if (this.cooldown <= 0) {
			if (this.firing) {
				this.shoot();

				this.cooldown = 10000;
			}
		}
	}

	onFixedUpdate(delta: number, absolute: number) {
		this.cooldown -= statList[StatTypes.SPEED][this.speedLevel];
	}

	onMessage(msg: CF.Message) {
		if (msg.action === CF.PointerMessages.POINTER_DOWN) {
			this.firing = true;
		}

		if (msg.action === CF.PointerMessages.POINTER_RELEASE) {
			this.firing = false;
		}

		if (msg.action === CF.PointerMessages.POINTER_TAP) {
			this.firing = false;
		}

		if (msg.action === CF.PointerMessages.POINTER_OVER) {
			this.owner.rotation = Math.atan2(msg.data.mousePos.posY - this.owner.position.y, msg.data.mousePos.posX - this.owner.position.x);
		}

		if (msg.action === Messages.UPGRADE_DAMAGE)
		{
			this.damageLevel++;
		}
		else if (msg.action === Messages.UPGRADE_SPEED)
		{
			this.speedLevel++;
		}
		else if (msg.action === Messages.UPGRADE_MULTISHOT)
		{
			this.multishotLevel++;
		}
		else if (msg.action === Messages.UPGRADE_KNOCKBACK)
		{
			this.knockbackLevel++;
		}

		this.checkForRank();
	}

	checkForRank() {
		let skillTotal = this.damageLevel + this.speedLevel + this.multishotLevel + this.knockbackLevel;

		if(skillTotal < 15)
		{
			this.rank = 0;
		}
		else if (skillTotal < 30)
		{
			this.rank = 1;
		}
		else 
		{
			this.rank = 2;
		}

		this.owner.assignAttribute("x", skinConfig[this.rank][1])
		this.owner.assignAttribute("y", skinConfig[this.rank][2])
		this.owner.assignAttribute("width", skinConfig[this.rank][3])
		this.owner.assignAttribute("height", skinConfig[this.rank][4])

		this.owner.asSprite().texture = PIXI.Texture.from(skinConfig[this.rank][0] as string);
	
		let tmp = this.owner.findComponentByName<AnimationController>("AnimationController").reload();
	}

	shoot() {
		this.sendMessage(Messages.NEW_PROJECTILE, {
			position: new CF.Vector(this.owner.position.x, this.owner.position.y),
			rotation: this.owner.rotation,
			damage: statList[StatTypes.DAMAGE][this.damageLevel],
			multishot: statList[StatTypes.MULTISHOT][this.multishotLevel],
			knockback: statList[StatTypes.KNOCKBACK][this.knockbackLevel],
			texture: skinConfig[this.rank][5] as string
		} as NewProjectileMessage);
	}
}