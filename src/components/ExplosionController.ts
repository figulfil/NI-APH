import * as CF from 'colfio';
import * as PIXI from 'pixi.js';

export class ExplosionController extends CF.Component {
  frame = 0;
  frameCounter = 0;

  onInit(): void {
    this.fixedFrequency = 32;
  }

  onFixedUpdate(delta: number, absolute: number): void {
    this.owner.asSprite().texture.frame = new PIXI.Rectangle(
        400 * (this.frame % 4), 400 * Math.floor(this.frame / 4), 400, 400
    );

    this.frame++;

    if(this.frame == 16)
        this.owner.destroy();
  }

  getFrameIndex(frame: number) {
    return frame < 3 ? frame : 6 - frame;
  }
}