export const SCENE_WIDTH = 16;
export const TEXTURE_SCALE = SCENE_WIDTH / (256 * 16);

export enum Assets {
    LEVEL_BACKGROUND = 'level_background',
    ENEMY_1 = 'enemy_1',
    ENEMY_2 = 'enemy_2',
    ENEMY_3 = 'enemy_3',
    ENEMY_4 = 'enemy_4',
    FRIENDLY_1 = 'FRIENDLY_1',
    FRIENDLY_2 = 'FRIENDLY_2',
    FRIENDLY_3 = 'FRIENDLY_3',
    PROJECTILE = 'prj',
    BUTTON = 'btn',
    COINS = 'coins',
    POUCH = 'pouch',
    LEVEL = 'level',
    LASER_1 = 'laser_1',
    LASER_2 = 'laser_2',
    LASER_3 = 'laser_3',
    HEARTH = 'hearth',
    EXPLOSION = 'explosion'
}

export enum Tags {
    BACKGROUND = 'background',
    PLAYER = 'player',
    ENEMY = 'enemey',
    PROJECTILE = 'projectile',
    BUTTON = 'button'
}

export enum Messages {
    COLLISION = 'collision',
    NEW_PROJECTILE = 'new_projectile',
    UPGRADE_DAMAGE = 'upgrade_damage',
    UPGRADE_SPEED = 'upgrade_speed',
    UPGRADE_MULTISHOT = 'upgrade_multishot',
    UPGRADE_KNOCKBACK = 'upgrade_knockback',
    INCOME = 'income',
    EXPENDITURE = 'expenditure',
    HEARTH_DOWN = 'hearth_down',
    GAME_OVER = 'game_over'
}

export enum StatTypes {
    DAMAGE = 'damage',
    SPEED = 'speed',
    MULTISHOT = 'multishot',
    KNOCKBACK = 'knockback'
}