import * as CF from 'colfio';
import * as PIXI from 'pixi.js';

export class AnimationController extends CF.Component {
  frame = 0;
  frameCounter = 0;

  x: number;
  y: number;
  width: number;
  height: number;

  onInit(): void {
    this.fixedFrequency = 6;

    this.reload();
  }

  onFixedUpdate(delta: number, absolute: number): void {
    this.owner.asSprite().texture.frame = new PIXI.Rectangle(
      this.x * (this.getFrameIndex(this.frame)), this.y, this.width, this.height
    );

    this.frame = (this.frameCounter++) % 6;
  }

  getFrameIndex(frame: number) {
    return frame < 3 ? frame : 6 - frame;
  }

  reload()
  {
    this.x = this.owner.getAttribute("x") as number;
    this.y = this.owner.getAttribute("y") as number;
    this.width = this.owner.getAttribute("width") as number;
    this.height = this.owner.getAttribute("height") as number;
  }
}