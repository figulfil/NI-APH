import * as CF from 'colfio';
import { Assets } from './Constants';
import { GameSystem } from './systems/GameSystem';
import { CollisionSystem } from './systems/CollisionSystem';
import { StatSystem } from './systems/StatSystem';

export abstract class GameLoader 
{
    public static loadGame(engine: CF.Engine) 
    {
        engine.app.loader
            .reset()
			.add(Assets.LEVEL_BACKGROUND, 'assets/space.png')
			.add(Assets.ENEMY_1, 'assets/enemy_1.png')
			.add(Assets.ENEMY_2, 'assets/enemy_2.png')
			.add(Assets.ENEMY_3, 'assets/enemy_3.png')
			.add(Assets.ENEMY_4, 'assets/enemy_4.png')
			.add(Assets.FRIENDLY_1, 'assets/friendly_1.png')
			.add(Assets.FRIENDLY_2, 'assets/friendly_2.png')
			.add(Assets.FRIENDLY_3, 'assets/friendly_3.png')
			.add(Assets.BUTTON, 'assets/button.png')
			.add(Assets.COINS, 'assets/coins.png')
			.add(Assets.POUCH, 'assets/pouch.png')
			.add(Assets.LEVEL, 'assets/level.txt')
			.add(Assets.LASER_1, 'assets/red_laser.png')
			.add(Assets.LASER_2, 'assets/blue_laser.png')
			.add(Assets.LASER_3, 'assets/green_laser.png')
			.add(Assets.HEARTH, 'assets/hearth.png')
			.add(Assets.EXPLOSION, 'assets/explosion.png')
			.load(() => this.onAssetsLoaded(engine));
    }

	private static onAssetsLoaded(engine: CF.Engine)
    {
		engine.scene.addGlobalComponent(new GameSystem());
    }
}