import * as CF from 'colfio';
import * as PIXI from 'pixi.js';
import { Assets, StatTypes, Messages, TEXTURE_SCALE, Tags } from '../Constants';
import { MoneyMessage } from '../Messages';
import { ButtonController } from '../components/ButtonController';

export class StatSystem extends CF.Component {
    money: number = 0;
    damage: number = 1000;
    speed: number = 10;
    multishot: number = 10;
    allies: number = 4;
    hearth: number = 100;
    score: number = 0;

    moneyValue: PIXI.Text;
    hitPointsValue: PIXI.Text;

    onInit() {
        this.loadMenu();
        this.loadMoney();
        this.loadButtons();
        this.loadHitPoints();

        this.subscribe(Messages.INCOME);
        this.subscribe(Messages.EXPENDITURE);

        this.subscribe(Messages.HEARTH_DOWN);
    }

    onMessage(msg: CF.Message) {
        if (msg.action === Messages.INCOME)
            this.updateMoney((msg.data as MoneyMessage).money);
        else if (msg.action === Messages.EXPENDITURE)
            this.updateMoney((msg.data as MoneyMessage).money);
        else if (msg.action === Messages.HEARTH_DOWN)
            this.updateHitPoints();
    }

    loadMenu() {
        const menu = new CF.Graphics();

        menu.beginFill(789009);
        menu.drawRect(0, 0, this.owner.width, 0.5);
        menu.endFill();

        this.owner.addChild(menu);
    }

    loadHitPoints() {
        const img = new PIXI.Sprite(PIXI.Texture.from(Assets.HEARTH));
        img.position.set(0.8, 0);
        img.scale.set(TEXTURE_SCALE * 2);
        this.owner.addChild(img);

        this.hitPointsValue = new PIXI.Text(this.hearth, { fill: 0xFFFFFF, fontSize: 26 });
        this.hitPointsValue.resolution = 1;
        this.hitPointsValue.scale.set(TEXTURE_SCALE * 4);
        this.hitPointsValue.position.set(0.4 - 0.2 * (this.hearth.toString().length - 1), 0);
        this.scene.stage.addChild(this.hitPointsValue);
    }

    updateHitPoints() {
        this.hearth--;

        if(this.hearth <= 0)
        {
            this.sendMessage(Messages.GAME_OVER);
        }

        this.hitPointsValue.text = this.hearth;
        this.hitPointsValue.position.set(0.4 - 0.2 * (this.hearth.toString().length - 1), 0);
    }

    loadMoney() {
        const img = new PIXI.Sprite(PIXI.Texture.from(Assets.POUCH));
        img.position.set(15.5, 0);
        img.scale.set(TEXTURE_SCALE * 7);
        this.owner.addChild(img);

        this.moneyValue = new PIXI.Text(this.money, { fill: 0xFFFFFF, fontSize: 26 });
        this.moneyValue.resolution = 1;
        this.moneyValue.scale.set(TEXTURE_SCALE * 4);
        this.moneyValue.position.set(15 - 0.2 * (this.money.toString().length - 1), 0);
        this.scene.stage.addChild(this.moneyValue);
    }

    updateMoney(money: number) {
        this.score += money;
        this.money += money;
        this.moneyValue.text = this.money;
        this.moneyValue.position.set(15 - 0.2 * (this.money.toString().length - 1), 0);
    }

    loadButtons() {
        let tmp = new CF.Builder(this.scene)
            .withName(Tags.BUTTON)
            .asSprite(PIXI.Texture.from(Assets.BUTTON))
            .scale(TEXTURE_SCALE * 4, TEXTURE_SCALE * 2)
            .localPos(0, 11.5)
            .withParent(this.scene.stage)
            .withComponent(new ButtonController())
            .withAttribute("text", "Damage")
            .withAttribute("type", StatTypes.DAMAGE)
            .withAttribute("func", () => this.scene.sendMessage(new CF.Message(Messages.UPGRADE_DAMAGE)))
            .build();

        new CF.Builder(this.scene)
            .withName(Tags.BUTTON)
            .asSprite(PIXI.Texture.from(Assets.BUTTON))
            .scale(TEXTURE_SCALE * 4, TEXTURE_SCALE * 2)
            .localPos(4, 11.5)
            .withParent(this.scene.stage)
            .withComponent(new ButtonController())
            .withAttribute("text", "Speed")
            .withAttribute("type", StatTypes.SPEED)
            .withAttribute("func", () => this.scene.sendMessage(new CF.Message(Messages.UPGRADE_SPEED)))
            .build();

        new CF.Builder(this.scene)
            .withName(Tags.BUTTON)
            .asSprite(PIXI.Texture.from(Assets.BUTTON))
            .scale(TEXTURE_SCALE * 4, TEXTURE_SCALE * 2)
            .localPos(8, 11.5)
            .withParent(this.scene.stage)
            .withComponent(new ButtonController())
            .withAttribute("text", "Multishot")
            .withAttribute("type", StatTypes.MULTISHOT)
            .withAttribute("func", () => this.scene.sendMessage(new CF.Message(Messages.UPGRADE_MULTISHOT)))
            .build();

        new CF.Builder(this.scene)
            .withName(Tags.BUTTON)
            .asSprite(PIXI.Texture.from(Assets.BUTTON))
            .scale(TEXTURE_SCALE * 4, TEXTURE_SCALE * 2)
            .localPos(12, 11.5)
            .withParent(this.scene.stage)
            .withComponent(new ButtonController())
            .withAttribute("text", "Repulse")
            .withAttribute("type", StatTypes.KNOCKBACK)
            .withAttribute("func", () => this.scene.sendMessage(new CF.Message(Messages.UPGRADE_KNOCKBACK)))
            .build();
    }
}