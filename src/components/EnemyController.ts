import * as CF from 'colfio';
import { MoneyMessage } from '../Messages';
import { Messages } from '../Constants';
import { Factory } from '../Factory';

export class EnemyController extends CF.Component {
	frame = 0;
	frameCounter = 0;

	health: number;
	speed: number;
	value: number;

	onInit() {
		this.fixedFrequency = 6;
		this.speed = this.owner.getAttribute("speed") as number;
		this.health = this.owner.getAttribute("health") as number;
		this.value = this.owner.getAttribute("value") as number;
	}

	onUpdate(delta: number, absolute: number): void {
		this.owner.position.x -= this.speed;
	}
	
	public takeHit(damage, knockback) {
		this.health -= damage;
		this.owner.position = new CF.Vector(this.owner.position.x + knockback / 10, this.owner.position.y);

		if (this.health <= 0) {
			this.sendMessage(Messages.INCOME, {
				money: this.value
			} as MoneyMessage);

            Factory.loadExplosion(this.scene, new CF.Vector(this.owner.position.x, this.owner.position.y));
			this.owner.destroy();
		}
	}
}