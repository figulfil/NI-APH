import * as CF from 'colfio';
import * as PIXI from 'pixi.js';
import { Messages, Tags } from '../Constants';
import { CollisionMessage } from '../Messages';

export class CollisionSystem extends CF.Component {

    onInit(): void {
        this.fixedFrequency = 60;
    }

    onFixedUpdate(delta: number, absolute: number): void {
        const projectiles = this.scene.findObjectsByName(Tags.PROJECTILE).reverse();
        const enemies = this.scene.findObjectsByName(Tags.ENEMY).reverse();

        for(let projectile of projectiles)
        {
            const pBox = projectile.getBounds();

            for(let enemy of enemies)
            {
                const eBox = enemy.getBounds();

                const horizIntersection = this.horizIntersection(pBox, eBox);
                const vertIntersection = this.vertIntersection(pBox, eBox);

                if (horizIntersection > 0 && vertIntersection > 0) {
                    this.sendMessage(Messages.COLLISION, {
                        projectile,
                        enemy
                    } as CollisionMessage);
                    
                    return;
                }
            }    
        }
    }

    private horizIntersection(boundsA: PIXI.Rectangle, boundsB: PIXI.Rectangle) {
        return Math.min(boundsA.right, boundsB.right) - Math.max(boundsA.left, boundsB.left);
    }

    private vertIntersection(boundsA: PIXI.Rectangle, boundsB: PIXI.Rectangle) {
        return Math.min(boundsA.bottom, boundsB.bottom) - Math.max(boundsA.top, boundsB.top);
    }
}