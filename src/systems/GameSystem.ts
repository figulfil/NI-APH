import * as CF from 'colfio';
import { Messages, Tags } from '../Constants';
import { CollisionMessage, NewProjectileMessage } from '../Messages';
import { Factory } from '../Factory';
import { EnemyController } from '../components/EnemyController';
import { ProjectileComponent } from '../components/ProjectileController';
import { CollisionSystem } from './CollisionSystem';
import { StatSystem } from './StatSystem';

const speedConfig = [100, 200, 400, 500, 600, 900, 1100, 1200, 2000, 2400, 3200, 3600, 4000, 8000, 9600, 11200, 12800, 14400, 16000];

export class GameSystem extends CF.Component {
    factory: Factory;

    private timeSinceLastIncrease: number = 0;
    private increaseInterval: number = 10000;
    private maximumFrequency = 5;
    private speedTick = 0;

    onInit() {
        this.scene.addGlobalComponent(new CF.PointerInputComponent({
            handlePointerRelease: true,
            handleClick: true,
            handlePointerDown: true,
            handlePointerOver: true
        }));

        this.scene.addGlobalComponent(new CollisionSystem());
        this.scene.addGlobalComponent(new StatSystem());

        this.fixedFrequency = 0.5;

        this.factory = new Factory(this.owner.scene);
        this.factory.loadGame();
        this.factory.loadCharacter(new CF.Vector(1, 6));

        this.subscribe(Messages.COLLISION);
        this.subscribe(Messages.NEW_PROJECTILE);
        this.subscribe(Messages.GAME_OVER);
    }

    onFixedUpdate(delta: number, absolute: number): void {
        this.timeSinceLastIncrease += delta;

        if (this.timeSinceLastIncrease >= this.increaseInterval) {
            if (this.fixedFrequency < this.maximumFrequency) {
                this.speedTick++;
            }
            
            this.fixedFrequency += 0.25;
            this.timeSinceLastIncrease = 0;
            console.log("Tick: " + this.speedTick + " Game speed: " + this.fixedFrequency + " Health: " + speedConfig[this.speedTick]);
        }

        this.factory.loadWave(this.fixedFrequency / 50, speedConfig[this.speedTick]);
        this.removeObjectsOutOfBounds();
    }

    onMessage(msg: CF.Message) {
        if (msg.action === Messages.COLLISION) {
            const payload = msg.data as CollisionMessage;

            const damage = payload.projectile.findComponentByName<ProjectileComponent>("ProjectileComponent").damage;
            const knockback = payload.projectile.findComponentByName<ProjectileComponent>("ProjectileComponent").knockback;
            payload.enemy.findComponentByName<EnemyController>("EnemyController").takeHit(damage, knockback);

            payload.projectile.destroy();
        }
        else if (msg.action === Messages.NEW_PROJECTILE) {
            const payload = msg.data as NewProjectileMessage;

            this.factory.loadProjectile(payload.position, payload.rotation, payload.multishot, payload.damage, payload.knockback, payload.texture);
        }
        else if (msg.action === Messages.GAME_OVER) {
            this.gameOver();
        }
    }

    private gameOver() {
        let score = this.scene.findGlobalComponentByName<StatSystem>("StatSystem").score;

        this.scene.callWithDelay(0, () => {
            Factory.loadGameOverScreen(this.scene, score);
        });
    }

    private removeObjectsOutOfBounds() {
        for (let projectile of this.scene.findObjectsByName(Tags.PROJECTILE))
            if (projectile.x > this.scene.width + 1 || -1 > projectile.position.x || projectile.y > this.scene.height + 1 || -1 > projectile.position.y)
                projectile.destroy();

        for (let enemy of this.scene.findObjectsByName(Tags.ENEMY))
            if (enemy.position.x < -1) {
                this.scene.sendMessage(new CF.Message(Messages.HEARTH_DOWN));
                enemy.destroy();
            }
    }
}